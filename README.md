# Delta Filter

## INTRODUCTION
Views provides ways of handling multivalue fields in the "FIELDS" section of
the Views UI, e.g. you can opt to display only yhe first and last delta of a
multivalue field. However, use of delta's in "FILTERS" is limited to simple
numerical filters, e.g. the value of delta IS EQUAL TO <some value>.
A common usecase is where you have multiple multivalue fields you want to
filter on having the same delta for each field. Offcourse you might model
this differently using (multiple) paragraphs but this is not always
possible and introduces significant overhead. This filter plugin searches
for multivalue fields in your view and makes them selectable to filter on having
the same delta.

## REQUIREMENTS
Technically there are not many requirements: offcourse the module depends on
Views.
Conceptually this module only makes sense if you have multiple multivalue fields
where all delta's have some meaningfull relation. In most use cases this is
true if you programmatically store data or if you changed the field layout.
  
## INSTALLATION
Install this module as usual, no external libraries required.

## CONFIGURATION
This module has no site wide config options.

## USAGE
Add a new view or build on an existing view by adding at least two multi
-value fields in your view. Make sure you unselect the option "Display all
values in the same row" under "Multiple Field Settings". For each multivalue
field also add the corresponding "delta" field in the view. You can select
"Exclude from display" but the field needs to be available for the query.

Add a new filter; you can select the "Custom" category of filters or search for
"Delta Filter". Select all or at least two of the available fields to get views
to filter by delta.

You can check if your filter is working by comparing a view result with and
without this filter: the view without this filter would produce the outer join
result of all available delta's (which can produce lots and lots of records if
you for example have four fields with dozens of values each!). If necessary you
can add the delta fields to the "FIELD" to check if each row produces identical
delta's.
