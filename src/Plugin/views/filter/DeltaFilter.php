<?php

namespace Drupal\delta_filter\Plugin\views\filter;

use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Filters multivalue fields to have the same delta.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("delta_filter")
 */
class DeltaFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function canExpose() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['fields'] = ['default' => []];

    return $options;
  }

  /**
   * Display the filter on the administrative summary.
   */
  public function adminSummary() {
    return '= multiple fields';
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $this->view->initStyle();

    // Allow to choose all fields as possible.
    if ($this->view->style_plugin->usesFields()) {
      $options = [];
      foreach ($this->view->display_handler->getHandlers('field') as $name => $field) {
        // Only allow delta fields.
        if ($field->realField === 'delta') {
          $options[$field->table] = $field->table . ' (delta)';
        }
      }
      if ($options) {
        $form['fields'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose fields to filter on having the same delta'),
          '#description' => $this->t("This filter compares delta's for multivalue fields."),
          '#multiple' => TRUE,
          '#options' => $options,
          '#default_value' => $this->options['fields'],
        ];
      }
      else {
        $form_state->setErrorByName('', $this->t('You have to add some fields to be able to use this filter.'));
      }
    }
  }

  /**
   * Add this filter to the query.
   *
   * The number of delta's is based on the fields in the form options.
   */
  public function query() {
    $this->ensureMyTable();
    $delta_fields = [];

    // Build array of fields needed for SQL.
    foreach ($this->view->field as $name => $field) {
      if ($field instanceof NumericField && $field->realField === 'delta' &&
        in_array($field->table, $this->options['fields'])) {
        $delta_fields[] = [
          'alias' => $field->table,
          'real' => $field->realField,
        ];
      }
    }

    // If delta_fields does not have multiple fields abondon.
    if (count($delta_fields) < 2) {
      return;
    }

    // Define filter group.
    $this->query->setWhereGroup('AND', 'DeltaFilters');

    // Build pieces of SQL based on number of delta_fields.
    $i = 0;
    $j = 1;
    do {
      $snippet[$i] = $delta_fields[$i]['alias'] . '.' . $delta_fields[$i]['real'] . ' ' . '=' . ' ' . $delta_fields[$j]['alias'] . '.' . $delta_fields[$j]['real'];
      $this->query->addWhereExpression('DeltaFilters', $snippet[$i]);
      $i++;
      $j++;
    } while ($j < count($delta_fields));
  }

}
